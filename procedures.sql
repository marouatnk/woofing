-- Register procedure

CREATE DEFINER=`user`@`localhost` PROCEDURE `add_user`(
	IN u_email VARCHAR(255), 
    IN u_first_name VARCHAR(255), 
    IN u_last_name VARCHAR(255), 
    IN u_password VARCHAR(255), 
    IN u_phone_number VARCHAR(255)
    IN u_gender TINYINT(1),
    IN u_roles VARCHAR(255)

)
BEGIN
	SET u_password = SHA2(u_password,512);
	INSERT INTO `user` 
	(
		email,
		first_name,
		last_name,
		password,
		phone_number,
        gender,
        roles
	) 
	VALUES 
	(
		u_email,
		u_first_name,
		u_last_name,
		u_password,
		u_phone_number,
        gender,
        roles
	);


-- Add event procedure

USE `woofing`$$
CREATE DEFINER=`user`@`localhost` PROCEDURE `add_event`(
	IN u_created_by_id INT, 
    IN u_title VARCHAR(255),
    IN u_description TEXT, 
    IN u_start_date DATE, 
    IN u_end_date DATE, 
    IN u_max_capacity TINYINT(4)
)
BEGIN
	INSERT INTO `event` 
    (
		owner_id,
        title,
        description,
        start_date,
        end_date,
        max_capacity
    ) 
    VALUES 
    (
		u_owner_id,
        u_title,
        u_description,
        u_start_date,
        u_end_date,
        u_max_capacity
        
	);


-- Assign an address to an event procedure

CREATE DEFINER=`user`@`localhost` PROCEDURE `assign_address_to_event`(
	IN u_event_id INT(11),
	IN u_street_number INT(30),
	IN u_street_name VARCHAR(255),
	IN u_city VARCHAR(255),
	IN u_zip_code VARCHAR(30),
	IN u_country VARCHAR(255)
)
BEGIN
	DECLARE var_address_id INT;
    
	INSERT INTO address
	(
		street_number,
        street_name,
        additional_information,
        city_name,
        zip_code,
        state_name,
        country_name
	) 
    VALUES 
    (
		u_street_number,
        u_street_name,
        u_additional_information,
        u_city_name,
        u_zip_code,
        u_state_name,
        u_country_name
    );
    SELECT LAST_INSERT_ID() INTO var_address_id  FROM address LIMIT 1;
    UPDATE `event` SET `address_id` = var_address_id WHERE `id` = u__event_id;
;

-- Update an address procedure

CREATE DEFINER=`user`@`localhost` PROCEDURE `change_event_address`(
	IN street_number INT(30),
	IN street_name VARCHAR(255),
	IN city VARCHAR(255),
	IN zip_code VARCHAR(30),
	IN country VARCHAR(255)
)
BEGIN
    DECLARE var_event_id INT;

	UPDATE  address
	    set street_number = NEW.street_number
		set street_name = NEW.street_name
        set country = NEW.country
        set city = NEW.city
        set zip_code = NEW.zip_code	
    INNER JOIN 
    `event`
    WHERE `event_id` = var_event_id;
;
