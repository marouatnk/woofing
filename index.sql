
-- Add new users :
CALL `woofing`.add_user(`maroua.tnk@domain.co`,`Maroua`,`Tnk`,`'ROLE_ADMIN`,`0758934213`);
CALL `woofing`.add_user(`salah.daoudi@domain.co`,`Salalh`,`Daoudi`,`'ROLE_ADMIN'`,`0758934215`);
CALL `woofing`.add_user(`flo.maurel@domain.co`,`Florian`,`Maurel`, 'ROLE_ADMIN');
CALL `woofing`.add_user(`roro.benaziza@domain.co`,`Redouane`,`Banaziza`,`'ROLE_MANAGER'`,`0758934214`);
CALL `woofing`.add_user(`elaa.tnk@domain.co`,`Teresa`,`Elaa`,`Tnk`,`'ROLE_VOLUNTEER'`);
CALL `woofing`.add_user(`françois.blanc@domain.co`,`François`,`Blanc`,`'ROLE_VOLUNTEER'`,`0758934216`);
CALL `woofing`.add_user(`clem.Causier@domain.co`,`Clément`,`Causier`,`ROLE_VOLUNTEER`,`0758934217`);

-- Création des événements
CALL woofing.new_event(1, `Chantier 01`, `Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups..`, `2021-01-08`, `2021-01-28`, 6);
CALL woofing.new_event(2, `Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.`, `2021-02-12`, `2021-02-19`, 3);
CALL woofing.new_event(4, `Chantier 02`, `Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.`, `2021-02-12`, `2021-02-19`, 3);

-- Création d'adresses liées aux événements.
CALL assign_address_to_event(1,"", "", "Chateau de Belinay", "Paulhac", "15430", "Cantal", "France");
CALL assign_address_to_event(2,"", "", "Chateau de Javon", "Lioux", "84220", "Cantal", "Provence-Alpes-Côte dʼAzur");
CALL assign_address_to_event(2,"664", "Chemin de Montlis", "", "Lioux", "69560", "Saint-Cyr-sur-le-Rhône", "Rhône");

